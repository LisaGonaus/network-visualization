// Data
var totalNodes = 0;

// misc variables
var i = 0;

var treeHeight = height;
var translation;
var diagonal;
var root;


// define the tree and sort the nodes based on group and within the group based on name
var tree = d3.layout.tree()
	.size([height, width]).sort(function(a,b){
      return  a.group.toLowerCase().localeCompare(b.group.toLowerCase()) * 100 + a.name.toLowerCase().localeCompare(b.name.toLowerCase());
    });

// Define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
var zoomListener = d3.behavior.zoom().translate([width * 0.1, 0]).on("zoom", zoom);


// Toggle children on click.
function deleteNode(id) {
	console.log("delete " + id);
	android.requestData(true);
}

function updateData(data) {
	json = JSON.parse(data);
	console.log("Data update");
	// define a d3 diagonal projection for use by the node paths later on.
	diagonal = d3.svg.diagonal()
		.projection(d => {
			return [d.y, d.x];
		});
	//x0 = root.x0;
	//y0 = root.y0;

	if (collapseInit) {
		root.children.forEach(d => {
			if (d.children != null && d.children.length != 0) {
				d._children = d.children;
				d.children = null;
			}
		});
	}
	
	root = json;
	root.x0 = height / 2;
	root.y0 = 0;
	update(root);
}


function init(data) {
	json = JSON.parse(data);

	// create the containerv svg with the base group (g)
	svg = d3.select("body").append("svg")
		.attr("width", width)
		.attr("height", height)
		.call(zoomListener)
		.append("g");

	// define a d3 diagonal projection for use by the node paths later on.
	diagonal = d3.svg.diagonal()
		.projection(d => {
			return [d.y, d.x];
		});

	// tree root
	root = json;
	root.x0 = height / 2;
	root.y0 = 0;

	if (collapseInit) {
		root.children.forEach(d => {
			if (d.children != null && d.children.length != 0) {
				d._children = d.children;
				d.children = null;
			}
		});
	}
	update(root);
	center(root);

}

function getNodeDepht(level, n) {
	if (n.children && n.children.length > 0) {
		if (levelWidth.length <= level + 1) levelWidth.push(0);

		levelWidth[level + 1] += n.children.length;
		n.children.forEach(function(d) {
			childCount(level + 1, d);
		});
	}
}

function update(source) {
	// Generate new Nodes
	var levelWidth = [1];
	var childCount = function(level, n) {
		if (n.children && n.children.length > 0) {
			if (levelWidth.length <= level + 1) levelWidth.push(0);

			levelWidth[level + 1] += n.children.length;
			n.children.forEach(function(d) {
				childCount(level + 1, d);
			});
		}
	};

	childCount(0, root);
	treeHeight = d3.max(levelWidth) * TEXT_OFFSET * 2; // 25 pixels per line  
	tree = tree.size([treeHeight, width]);


	//console.log("Update");
	var nodes = tree.nodes(root),
		links = tree.links(nodes);

	//----------------- Nodes ------------------//

	// Normalize for fixed-depth.
	nodes.forEach(d => {
		d.y = d.depth * DEPTH;
	});

	// Declare the nodes
	var node = svg.selectAll("g.node")
		.data(nodes, d => {
			return d.node_id || (d.node_id = ++i);
		});

	// Enter any new nodes at the parent's previous position.
	var nodeEnter = node.enter().append("g")
		.attr("class", "node")
		.attr("transform", function(d) {
			return "translate(" + source.y0 + "," + source.x0 + ")";
		})
		.on("click", click);

	// draw the circles
	nodeEnter.append("circle")
		.attr("r", CIRCLE_SIZE)
		.style("stroke", d => {
			var c = color(d.group);
			return d != selected_node ? lightenColor(c) : c;
		});

	nodeEnter.append("text")
		.attr("text-anchor", "middle")
		.attr("dy", ".35em")
		.style("fill", "white")
		.text(d => { return d._children ? d._children.length : ""});

	// add the names
	nodeEnter.append("text")
		.attr("class","label")
		.attr("x", d => {
			return d.children || d._children ? -TEXT_OFFSET : TEXT_OFFSET;
		})
		.style("font-size","14px")
		.style("fill-opacity", 1)
		.attr("dy", ".35em")
		.attr("text-anchor", function(d) {
			return d.children || d._children ? "end" : "start";
		})
		.text(d => {
			return d.name;
		});


	// update the node description weight
	node.select(".label")
		.style("font-weight", d => {
			return d==selected_node ? 700 : 300;
		});

	// Transition nodes to their new position.
	var nodeUpdate = node.transition()
		.duration(DURATION)
		.attr("transform", d => {
			return "translate(" + d.y + "," + d.x + ")";
		});

	// Update the circles
	nodeUpdate.select("circle")
		.attr("r", CIRCLE_SIZE)
		.style("fill", d => {
			var c = color(d.group);
			c = d != selected_node ? lightenColor(c) : c;
			return d._children ? c : "#fff";
		}).style("stroke", d => {
			var c = color(d.group);
			return d != selected_node ? lightenColor(c) : c;
		});


	nodeUpdate.select("text")
		.style("fill", d => {
			var c = color(d.group);
			c = d != selected_node ? lightenColor(c) : c;
			return d._children ? "#fff" : c;
		});

	// Transition exiting nodes to the parent's new position.
	var nodeExit = node.exit().transition()
		.duration(DURATION)
		.attr("transform", function(d) {
			return "translate(" + source.y + "," + source.x + ")";
		})
		.remove();

	// node exit
	nodeExit.select("circle")
		.attr("r", 1e-6);

	nodeExit.select("text")
		.style("fill-opacity", 1e-6);

	//----------------- Links ------------------//

	// draw the links
	var link = svg.selectAll("path.link")
		.data(links, function(d) {
			return d.target.node_id;
		});

	// Enter any new links at the parent's previous position.
	link.enter().insert("path", "g")
		.attr("class", "link")
		.attr("d", function(d) {
			var o = {
				x: source.x0,
				y: source.y0
			};
			return diagonal({
				source: o,
				target: o
			});
		});

	// Transition links to their new position.
	link.transition()
		.duration(DURATION)
		.attr("d", diagonal);

	// Transition exiting nodes to the parent's new position.
	link.exit().transition()
		.duration(DURATION)
		.attr("d", function(d) {
			var o = {
				x: source.x,
				y: source.y
			};
			return diagonal({
				source: o,
				target: o
			});
		})
		.remove();

	// Stash the old positions for transition.
	nodes.forEach(function(d) {
		d.x0 = d.x;
		d.y0 = d.y;
	});
}

function click(d) {
	console.log(d.id + " = " + d.name)
	if (d.children) {
		d._children = d.children;
		d.children = null;
	} else if (d._children) {
		d.children = d._children;
		d._children = null;
	}
	selected_node = d;
	update(d);
	center(d);

	android.selectNode(d.id);

	//android.deselectNode();
	//android.selectNode(d.id);
}

// center node, x position is chosen so that the previous level is still visible
function center(node) {
	var translate = [
		(DEPTH + TEXT_OFFSET - node.y) * scaleFactor,
		height / 2 - node.x * scaleFactor
	];

	svg.transition().duration(DURATION)
		.call(zoomListener.translate(translate).event);
}