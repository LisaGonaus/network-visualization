var color = d3.scaleOrdinal(d3.schemeCategory10);
const COLOR_NEUTRAL = "lightgray";
const LINK_COLOR = "lightgray";
const LINK_HIGHLIGHT = "#ff7f0e";
var selectedNode;

var width = window.innerWidth,
    height = window.innerHeight;

var diameter = 900,
    radius = diameter / 2,
    innerRadius = radius - 120;

var initialZoom = true;

var zoomListener = d3.zoom().on("zoom", zoom);

var cluster = d3.cluster()
    .size([360, innerRadius]);

var line = d3.radialLine()
    .curve(d3.curveBundle.beta(0.85))
    .radius(function(d) { 
    	return d.y; 
    	})
    .angle(function(d) { 
    	return d.x / 180 * Math.PI; 
    	});

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height)
    .call(zoomListener)
    .append("g")
    .attr("transform", "translate("+width/2+","+height/2+")");

var link = svg.append("g").selectAll(".link"),
    node = svg.append("g").selectAll(".node");

var links = [], nodes = [];

function init(data){
  var json = JSON.parse(data);

  // define the hierarchy and sort based on group and within the group based on name
  root = d3.hierarchy(json).sort(function(a,b){
      return a.data.group.toLowerCase().localeCompare(b.data.group.toLowerCase()) * 100 + a.data.name.toLowerCase().localeCompare(b.data.name.toLowerCase());
    });
  cluster(root);

  // format link data
  for(var i = 0; i< root.leaves().length; i++){
      links[i] = root.leaves()[i].path(root);
  }

  // get node data
  root.each(d =>{
    nodes.push(d);
  });

  update();

}

function update(){

  link = link.data(links, d => {return d[0] + "-" + d[d.size-1]});

  // enter links
  link.enter().append("path")
      .each(function(d) { d.source = d[0], d.target = d[d.length - 1]; })
      .attr("class", "link")
      .attr("d", line)
      .attr("stroke", d => {d.includes(selectedNode) ? LINK_HIGHLIGHT : LINK_COLOR});


  // node enter
  node = node.data(nodes).enter().append('g')
  		.attr("dy", "0.31em")
      .attr("transform", function(d) { 
          if(d.parent == null) {
            return "";
          } 
          else {
            return "rotate(" + (d.x - 90) + ")translate(" + (d.y + 8) + ",0)" + (d.x < 180 ? "" : "rotate(180)"); 
          }})
      .on("click", click);

  node.append("circle")
      .attr("r",d=>{return d.parent==null ? 15 : 0})
      .attr("fill", d=>{return color(d.data.group)});

  node.append("text").attr("class", "node")
      .attr("text-anchor", function(d) { 
        if(d.parent == null){
          return "middle";
        }else{
          return d.x < 180 ? "start" : "end"; 
        }
      })
      .attr("dy", ".35em")
      .style("font-weight", d=>{selectedNode == d ? "900" : "300"})
      .style("fill", d => {return d.parent==null ? "white" : color(d.data.group)})
      .text(function(d) { return d.parent==null ? "You" : d.data.name; })
}


function click(d) {
  if (selectedNode == d) {
    d3.selectAll(".link").style("stroke", LINK_COLOR);
    d3.selectAll(".node").style("font-weight", "300")
      .style("fill", d => {
        return d.parent == null ? "white" : color(d.data.group)
      })
    selectedNode = null;
    android.deselctNode();
  } else {
    d3.selectAll(".link").style("stroke", link => {
      for (var i = 0; len = link.length, i < len; i++) {
        if (link[i].data.id == d.data.id) {
          return LINK_HIGHLIGHT;
        }
      }
      return LINK_COLOR;
    });
    // .classed("link--target", function(l) { if (l.includes(d)) return l.source.source = true; })
    //.filter(function(l) { return l.target === d || l.source === d; })
    //.raise();
    var n = d3.selectAll(".node").style("font-weight", node => {
      return d == node ? "600" : "300";
    }).style("fill", d => {
      return d.parent == null ? "white" : color(d.data.group)
    });
    if (d.data != root.data) {
      n.style("fill", node => {
        if (node.parent == null) {
          return "white";
        }
        var isConnected = false;
        if (node.data.id == d.data.id || node.parent.data.id == d.data.id) {
          isConnected = true;
        }
        if (node.children != null && node.children.find(child => {
            return child.data.id == d.data.id
          }) != null) {
          isConnected = true;
        }
        if (d == node || d.parent == node || (d.children != null && d.children.includes(node))) {
          isConnected = true;
        }
        return isConnected ? color(node.data.group) : LINK_COLOR;
      })
    }

    selectedNode = d;
    android.selectNode(d.data.id);
  }
  
  update();
/*
  node.style("fill", node => { 
        return node==d ? color(node.data.group) : LINK_COLOR
      });
  */    
}

// handler for Zooming & Panning
function zoom() {
  //scaleFactor = d3.event.scale;
  svg.attr("transform", d3.event.transform);  
}