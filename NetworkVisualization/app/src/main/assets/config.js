// --------------------------------------------------------
// General settings
var svg;
var json;

// svg size
var width = window.innerWidth,
  height = window.innerHeight;

// display configuration
var color = d3.scale.category10();
const COLOR_NEUTRAL = "lightgray";
const LINK_COLOR = "lightgray";
const LINK_HIGHLIGHT = "#ff7f0e";
var LINK_WEIGHT = 10;

// Node Settings
const CIRCLE_SIZE = 12;
const TEXT_SIZE = "12px";

// Transition settings
var DURATION = 750;

// Data
var selected_node = null;

// ------------------------------------------------------------------------------------------------------------------------------------------------//
// Chord settings
const TENSION = 85;

// ------------------------------------------------------------------------------------------------------------------------------------------------//
// Force settings
const CHARGE = -300;
const LINK_STRENGTH = 1;
const MIN_DIST = 100;
const LINK_WEIGHT_FACTOR = 4;

// ------------------------------------------------------------------------------------------------------------------------------------------------//
// Tree settings
const DEPTH = 100;
const TEXT_OFFSET = CIRCLE_SIZE + 6;
const COLLAPSE_DEPTH = 1;

var collapseInit = true;
var scaleFactor = 1;

// ------------------------------------------------------------------------------------------------------------------------------------------------//
// General functions

// handler for Zooming & Panning
function zoom() {
  scaleFactor = d3.event.scale;
  svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
}

// ------------------------------------------------------------------------------------------------------------------------------------------------//
// Color manipulation

function lightenColor(color) {
  return shadeColor(color, 0.5);
}

function shadeColor(color, percent) {
  var f = parseInt(color.slice(1), 16),
    t = percent < 0 ? 0 : 255,
    p = percent < 0 ? percent * -1 : percent,
    R = f >> 16,
    G = f >> 8 & 0x00FF,
    B = f & 0x0000FF;
  return "#" + (0x1000000 + (Math.round((t - R) * p) + R) * 0x10000 + (Math.round((t - G) * p) + G) * 0x100 + (Math.round((t - B) * p) + B)).toString(16).slice(1);
}

function blendColors(c0, c1, p) {
  var f = parseInt(c0.slice(1), 16),
    t = parseInt(c1.slice(1), 16),
    R1 = f >> 16,
    G1 = f >> 8 & 0x00FF,
    B1 = f & 0x0000FF,
    R2 = t >> 16,
    G2 = t >> 8 & 0x00FF,
    B2 = t & 0x0000FF;
  return "#" + (0x1000000 + (Math.round((R2 - R1) * p) + R1) * 0x10000 + (Math.round((G2 - G1) * p) + G1) * 0x100 + (Math.round((B2 - B1) * p) + B1)).toString(16).slice(1);
}