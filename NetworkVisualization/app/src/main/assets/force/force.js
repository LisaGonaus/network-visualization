// TODO: node grouping
// var grouped <- index = group, content = nodes
// var group_links <- links between groups, strenght = link_count = sum(link.weight)

var force = {};
var linkedNodes = {};
var nodesById = {};

// Define the zoomListener which calls the zoom function on the "zoom" event
var zoomListener = d3.behavior.zoom().on("zoom", zoom);

svg = d3.select("body").append("svg")
  .attr("width", width)
  .attr("height", height)
  .call(d3.behavior.zoom().on("zoom", zoom))
  .append("g");

svg.append("g").attr("class","links");
svg.append("g").attr("class","nodes");

// Reverts back to the default display style
function clearHighlight(d) {
  d3.selectAll(".node").select("path").style("fill", function(circle) {
    return color(circle.group);
  });
  d3.selectAll("line")
    .style("opacity", 1)
    .style("stroke", LINK_COLOR);
}

// Highlights the selected node and it's links
// Hides other node labels
function highlight(d) {
  console.log("mouseover: " + d.name);

  d3.selectAll(".link").style("opacity", function(link) {
    //return link.source == d || link.target == d ? 1 : 0.25
    return link.source.id == d.id || link.target.id == d.id ? 1 : 0.25;
  }).style("stroke", function(link) {
    return link.source.id == d.id || link.target.id == d.id ? LINK_HIGHLIGHT : LINK_COLOR
  });

  d3.selectAll(".node").select("path").style("fill", function(node) {
    var isLinked = linkedNodes[d.id + "," + node.id] || linkedNodes[node.id + "," + d.id];
    return node.id == d.id || isLinked ? color(node.group) : COLOR_NEUTRAL;
  });
  //android.showDetails(d.id);
}

function center(node) {
  var translate = [
    (width/2 - node.y) * scaleFactor,
    (height/2 - node.x) * scaleFactor
  ];

  svg.transition().duration(DURATION)
    .call(zoomListener.translate(translate).event);
}

function click(d) {
  if (d != selected_node) {
    console.log("click [id: " + d.id + ", index: " + d.index + " = " + d.name);
    selected_node = d;
    highlight(d);
    android.selectNode(d.id);
  } else {
    // debug:
    //deleteNode(selected_node.id);
    selected_node = null;
    clearHighlight();
    android.deselectNode();
  }
}

function deleteNode(id) {
  var d = nodesById[id];
  json.nodes.splice(d.index, 1);
  json.links = json.links.filter(link => {
    return link.source.id !== d.id && link.target.id !== d.id;
  });
  clearHighlight();
  update();
  console.log("Delete " + id + " = " + d.id + ", name = " + d.name);
}

function deleteLink(source, target) {
  json.links = json.links.filter(link => {
    return !((link.source.id == source && link.target.id == target) || (link.source.id == target && link.target.id == source));
  });
  clearHighlight();
  update();
  console.log("Delete Link: " + id1 + " --> " + id2);
}

// initializes the force layout
function init(data) {
  json = JSON.parse(data);
  // initializing the node position at the center of the view 
  // (to stabilize the initial layouting)
  json.nodes.forEach(d => {
    if(d.id == json.root){
      d.x = width / 2 ;
      d.y = height / 2 ;
    }else{
      d.x = width / 2 + Math.random() * 100 - 50;
      d.y = height / 2 + Math.random() * 100 - 50;
    }
  });

  for (var i = 0, len = json.nodes.length; i < len; i++) {
    nodesById[json.nodes[i].id] = json.nodes[i];
  }

  json.links.forEach(function(d) {
    if (typeof d.source == "number") {
      d.source = nodesById[d.source];
    }
    if (typeof d.target == "number") {
      d.target = nodesById[d.target];
    }
    if (d.weight >= LINK_WEIGHT) {
      LINK_WEIGHT = d.weight + 1;
    }
  });

  force = d3.layout.force()
    //.gravity(.05)
    .charge(CHARGE)
    .linkStrength(LINK_STRENGTH)
    .size([width, height]);

  update();
}

function update() {

  // updates the link and node loookup tables
  for (var i = 0, len = json.nodes.length; i < len; i++) {
    nodesById[json.nodes[i].id] = json.nodes[i];
  }

  for (var i = 0, len = json.links.length; i < len; i++) {
    linkedNodes[json.links[i].source.id + "," + json.links[i].target.id] = true;
  }

  force
    .nodes(json.nodes)
    .links(json.links)
    .distance(d => {
      return d.weight * LINK_WEIGHT_FACTOR + MIN_DIST;
      //return 100;
    })
    .start();

  // ---------------- Links ----------------------//
  var link = svg.select(".links").selectAll(".link")
    .data(json.links, function(d) {
      return d.target.id + "-" + d.source.id;
    });

  // enter
  link.enter().append("line")
    .attr("class", "link")
    .style("stroke-width", d => {
      return Math.sqrt((LINK_WEIGHT - d.weight));
    })
    .style("color", LINK_COLOR);

  // exit
  link.exit().remove();

  // ---------------- Nodes ----------------------//
  var node = svg.select(".nodes").selectAll(".node")
    .data(json.nodes, function(d) {
      return d.id;
    });

  // enter
  var nodeEnter = node.enter().append("g")
    .attr("class", "node")
    .on("click", click)
    //.call(force.drag);

  nodeEnter.append("path")
    .attr("d", d => {
      var symbol = d3.svg.symbol();
      symbol.size(Math.pow(CIRCLE_SIZE*2,2));
      if( d.id == json.root ){
        symbol.type(d3.svg.symbolTypes[3]);
      } 
      return symbol();
    })
    .attr("transform", d => { return d.id == json.root ? "rotate(45)" : "rotate(0)";})
    .style("fill", d => {
      return color(d.group);
    });

  nodeEnter.append("text")
    .attr("dy", ".35em")
    .attr({
      'text-anchor': 'middle'
    })
    .text(d => {
      return d.name[0].toUpperCase();
    });

  // exit
  node.exit()
    .remove();

  force.on("tick", function() {

    node.attr("transform", d => {
      return "translate(" + d.x + "," + d.y + ")";
    });

    link.attr("x1", d => {
        return d.source.x;
      })
      .attr("y1", d => {
        return d.source.y;
      })
      .attr("x2", d => {
        return d.target.x;
      })
      .attr("y2", d => {
        return d.target.y;
      });
  });

  // debug
  console.log("update");
}