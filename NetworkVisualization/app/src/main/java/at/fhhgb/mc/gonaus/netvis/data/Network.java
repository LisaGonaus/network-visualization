package at.fhhgb.mc.gonaus.netvis.data;

import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Queue;

/**
 * This class stores a specific network.
 * Created by Lisa on 22.05.2017.
 */
public class Network {
    private Node root;
    private Hashtable<Integer, Node> nodes = new Hashtable<Integer, Node>();
    private ArrayList<Link> links = new ArrayList<>();

    public void setRoot(@NonNull Node p) {
        root = p;
        DataSource.getInstance().hasChanged = true;
    }

    public void addPerson(@NonNull Node p) {
        getNodes().put(p.getId(), p);
        DataSource.getInstance().hasChanged = true;
    }

    public Node getNode(int id) {
        return getNodes().get(id);
    }

    /**
     * @return A json string representation of the network in the format of an adjacency list
     */
    public String getJson() {

        try {
            JSONObject json = new JSONObject();

            ArrayList<Integer> keys = new ArrayList<>(this.getNodes().keySet());
            Collections.sort(keys);

            if (root == null) {
                Log.w("Network","No Root Node found");
                root = getNodes().get(keys.get(0));
            }
            json.put("root", root.getId());

            JSONArray nodes = new JSONArray();
            for (int i = 0; i < keys.size(); i++) {
                JSONObject node = new JSONObject();
                Node person = this.getNodes().get(keys.get(i));
                node.put("id", person.getId());
                node.put("name", person.getData("name"));
                if (person.getData("group").isEmpty()) {
                    person.setData("group", "" + Math.ceil(Math.random() * 5));
                }
                node.put("group", person.getData("group"));
                nodes.put(i, node);
            }
            json.put("nodes", nodes);

            JSONArray links = new JSONArray();
            for (int i = 0; i < getLinks().size(); i++) {
                JSONObject link = new JSONObject();
                Link con = getLinks().get(i);
                if(con.getNode1() != null && con.getNode2()!=null){
                    link.put("source", con.getNode1().getId());
                    link.put("target", con.getNode2().getId());
                    if (con.weight == 0) {
                        con.weight = (int) Math.ceil(Math.random() * DataSource.maxWeight);
                    }
                    link.put("weight", con.getWeight());
                    links.put(i, link);
                }
            }
            json.put("links", links);
            return json.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * A function that searches for all links that contain a node with the given id.
     * @param id the id of the node
     * @param visited all ids of the nodes that should be excluded from the result
     * @return a list of all found links
     */
    public ArrayList<Link> getLinks(int id, ArrayList<Integer> visited) {
        ArrayList<Link> nodeLinks = new ArrayList<Link>();
        for (Link link : getLinks()) {
            if (link.contains(id) && !link.containsIds(visited)) {
                nodeLinks.add(link);
            }
        }
        return nodeLinks;
    }

    private void getChildren(JSONObject root) throws JSONException {

        // contains all visited nodes
        ArrayList<Integer> visited = new ArrayList<>();
        // contains the current parent nodes
        Queue<JSONObject> parentNodes = new ArrayDeque<>();
        parentNodes.add(root);

        // find all direct children to the parent
        while (!parentNodes.isEmpty()) {
            // fetch the parent from the queue
            JSONObject parent = parentNodes.poll();
            int pID = parent.getInt("id");

            // fetch the links that don't contain already visited nodes
            ArrayList<Link> res = getLinks(pID, visited);

            // initializes a list containing the current children
            JSONArray children = new JSONArray();
            // generate all children and add them to the queue
            for (int i = 0; i < res.size(); i++) {
                //itterate through children
                Node current = res.get(i).getNode1().getId() == pID ? res.get(i).getNode2() : res.get(i).getNode1();
                JSONObject child = new JSONObject();
                child.put("id", current.getId());
                child.put("name", current.getData("name"));
                child.put("group", current.getData("group"));
                child.put("parent", pID);
                visited.add(pID);
                children.put(i, child);
                // add to tmp
                parentNodes.add(child);
            }

            // add children to root node in the first run
            parent.put("children", children);
            // add node to visited
            visited.add(pID);
        }

    }

    /**
     * @return A json string representation of the network in the format of a parent child hirachy (minimum spanning tree)
     */
    String getTreeJson() {
        try {
            JSONObject json = new JSONObject();
            if (getRoot() == null && getNodes().size() > 0) {
                setRoot(getNodes().get(0));
            }
            if (getRoot() != null) {
                json.put("id", getRoot().getId());
                json.put("name", getRoot().getData("name"));
                json.put("group", getRoot().getData("group"));
                json.put("parent", "null");
                getChildren(json);
            }
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public Node getRoot() {
        return root;
    }

    /**
     * Removes a given node from the network
     * @param node
     */
    public void deleteNode(Node node) {
        ArrayList<Link> cons = getLinks(node.getId(), null);
        for (Link con : cons) {
            getLinks().remove(con);
        }
        getNodes().remove(node.getId());
        DataSource.getInstance().hasChanged = true;
    }

    /**
     * @return true if the network is empty
     */
    public boolean isEmpty() {
        return getNodes().size() == 0 ? true : false;
    }

    public Hashtable<Integer, Node> getNodes() {
        return nodes;
    }

    public void setNodes(Hashtable<Integer, Node> nodes) {
        this.nodes = nodes;
    }

    public ArrayList<Link> getLinks() {
        return links;
    }

}
