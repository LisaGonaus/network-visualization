package at.fhhgb.mc.gonaus.netvis.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import at.fhhgb.mc.gonaus.netvis.PathViewActivity;
import at.fhhgb.mc.gonaus.netvis.R;
import at.fhhgb.mc.gonaus.netvis.data.DataSource;
import at.fhhgb.mc.gonaus.netvis.data.Route;

/**
 * Created by Lisa on 17.06.2017.
 * This Adapter displays multiple paths in a list of cardviews.
 */

public class MultiplePathAdapter extends RecyclerView.Adapter<MultiplePathAdapter.ViewHolder> {
    private Route[] mRoutes;
    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout mContent;
        public ViewHolder(LinearLayout v) {
            super(v);
            mContent = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MultiplePathAdapter(Context c) {
        mRoutes = DataSource.getInstance().currentRoutes.toArray(new Route[]{});
        mContext = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MultiplePathAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card, parent, false);
        // set the view's size, margins, paddings and layout parameters
        //...
        RecyclerView pathView = (RecyclerView)v.findViewById(R.id.card_content);
        pathView.setLayoutManager(new LinearLayoutManager(mContext));

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        RecyclerView pathView = (RecyclerView) holder.mContent.findViewById(R.id.card_content);
        pathView.setAdapter(new PathAdapter(mRoutes[position],mContext));

        holder.mContent.setClickable(true);
        holder.mContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRouteDetails(v);
            }
        });

    }

    private void showRouteDetails(View v) {
        PathViewActivity activity = (PathViewActivity) mContext;
        RecyclerView path = (RecyclerView)activity.findViewById(R.id.path_view);
        int itemPosition = path.getChildLayoutPosition(v);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mRoutes.length;
    }
}

