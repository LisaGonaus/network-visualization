package at.fhhgb.mc.gonaus.netvis.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

import at.fhhgb.mc.gonaus.netvis.R;
import at.fhhgb.mc.gonaus.netvis.*;
import at.fhhgb.mc.gonaus.netvis.data.Link;
import at.fhhgb.mc.gonaus.netvis.data.DataSource;
import at.fhhgb.mc.gonaus.netvis.data.Node;

/**
 * Created by Lisa on 17.06.2017.
 * An Adapter used to display a List of Nodes with the possibility to add/remove them as Contacts.
 */
public class AddContactAdapter extends RecyclerView.Adapter<AddContactAdapter.ViewHolder> {
    private Node mNode;
    private ArrayList<Link> mLinks;
    private int numberOfVisibleContacts = 10;
    private AddNodeActivity mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout mContent;
        public ViewHolder(LinearLayout v) {
            super(v);
            mContent = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AddContactAdapter(final Node node, AddNodeActivity c, final Hashtable<Integer, Node> contacts) {
        mNode = node;
        mLinks = new ArrayList<>();
        final ArrayList<Integer> keys = new ArrayList<>(contacts.keySet());
        Collections.sort(keys);

        for(int key : keys){
            mLinks.add(new Link(node,contacts.get(key),-1));
        }
        //final AddContactAdapter adapter = this;
        mContext = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AddContactAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_node_data_entry, parent, false);

        // set the view's size, margins, paddings and layout parameters
        //...
        AddContactAdapter.ViewHolder vh = new AddContactAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final AddContactAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Link con = mLinks.get(position);
        Node n = con.getContact(mNode);

        TextView name = (TextView) holder.mContent.findViewById(R.id.add_relation_node);

        name.setText(n.getData("name"));

        final ImageButton btn = (ImageButton) holder.mContent.findViewById(R.id.add_relation_btn_toggle);
        final ConstraintLayout weightLayout = (ConstraintLayout)  holder.mContent.findViewById(R.id.add_relation_weight);
        final SeekBar weightSeeker = (SeekBar) weightLayout.findViewById(R.id.add_relation_seek_weight);
        weightSeeker.setMax(DataSource.maxWeight-1);

        weightSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                con.setWeight(seekBar.getMax() - progress + 1);
                TextView status = (TextView)weightLayout.findViewById(R.id.add_relation_status);
                status.setText(DataSource.getInstance().getLinkTypeFromValue(con));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        if(con.getWeight() != -1){
            btn.setRotation(45);
            weightLayout.setVisibility(View.VISIBLE);
            weightSeeker.setProgress(con.getWeight());
        } else{
            btn.setRotation(0);
            weightLayout.setVisibility(View.GONE);
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn.getRotation() == 0){
                    con.setWeight(weightSeeker.getMax() - weightSeeker.getProgress());
                    mContext.addLink(con);
                    weightLayout.setVisibility(View.VISIBLE);
                    btn.setRotation(45);

                }else{
                    mContext.removeLink(con);
                    weightLayout.setVisibility(View.GONE);
                    btn.setRotation(0);
                }
            }
        });

        if(position == numberOfVisibleContacts-5 && numberOfVisibleContacts < mLinks.size()) {
            numberOfVisibleContacts += numberOfVisibleContacts;
        }
        Log.d("Network","visible contacts: " + numberOfVisibleContacts + " of " + mLinks.size());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if(numberOfVisibleContacts< mLinks.size()){
            return numberOfVisibleContacts;
        }else{
            return mLinks.size();
        }
    }
}

