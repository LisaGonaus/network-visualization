package at.fhhgb.mc.gonaus.netvis.data;

/**
 * Created by Lisa on 19.06.2017.
 * An interface that needs to be implemented by a class if it want's to register as an observer of the DataSource.
 */
public interface DataObserver {
    /**
     * Called when a node has been deleted.
     * @param id id of the deleted node
     */
    void nodeDeleted(int id);
    /**
     * Called when a link has been deleted.
     * @param id1 id of the first node in the link
     * @param id2 id of the second node in the link
     */
    void linkDeleted(int id1, int id2);
    /**
     * Called when a link has been added.
     * @param id1 id of the first node in the link
     * @param id2 id of the second node in the link
     */
    void linkAdded(int id1, int id2);

    /**
     * Is called when a bigger change in the data has happend (eg. new network was loaded)
     */
    void dataChanged();
}
