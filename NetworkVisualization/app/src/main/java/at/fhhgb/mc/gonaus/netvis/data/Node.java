package at.fhhgb.mc.gonaus.netvis.data;

import android.support.annotation.NonNull;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * Created by Lisa on 22.05.2017.
 */

public class Node implements Comparable<Node>{
    static int nodeCount = 0;
    /**
     * The id of the node.
     */
    private int id;
    /**
     * Stores all additional data of the node.
     */
    private Hashtable<String, String> data = new Hashtable<String, String>();

    /**
     * Initializes a node with a specific name and id
     * @param id
     * @param name
     */
    public Node(int id, String name){
        if(nodeCount <= id) nodeCount = 1 + id;
        this.id = id;
        this.setData("name", name);
    }

    public Node(){
        id = nodeCount++;
    }

    /**
     * Compares the node with another one
     * @param other the other node
     * @return 0 if they are equal, -1 otherwise
     */
    @Override
    public int compareTo(@NonNull Node other) {
        if(other.id == id){
            return 0;
        }else{
            return -1;
        }
    }

    public int getId(){
        return id;
    }

    /**
     * Returns the data stored at a specific key.
     * @param key key to be read
     * @return the value stored at the key
     */
    public String getData(String key) {
        return data.get(key);
    }

    /**
     * Stores the given data.
     * @param key the key where it will be stored
     * @param data the data to store
     */
    public void setData(String key, String data) {
        this.data.put(key,data);
    }

    public Hashtable<String, String> getData(){
        return data;
    }

    @Override
    public String toString() {
        return "[" + getId() + "] " + getData("name");
    }

    public void setId(int id) {
        if(nodeCount <= id) nodeCount = 1 + id;
        this.id = id;
    }
}
