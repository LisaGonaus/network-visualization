package at.fhhgb.mc.gonaus.netvis;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;

import at.fhhgb.mc.gonaus.netvis.adapter.AddContactAdapter;
import at.fhhgb.mc.gonaus.netvis.data.Link;
import at.fhhgb.mc.gonaus.netvis.data.DataSource;
import at.fhhgb.mc.gonaus.netvis.data.Node;

/**
 * This activity is used for the creation of a new contact/node.
 */
public class AddNodeActivity extends AppCompatActivity {
    RecyclerView contactView;
    /**
     * Temporary node.
     */
    Node node;
    /**
     * Temporary list of links for the node.
     */
    ArrayList<Link> links;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_node);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        node = new Node();
        links = new ArrayList<>();

        contactView = (RecyclerView) findViewById(R.id.rv_node_contacts);

        Hashtable<Integer,Node> nodes = DataSource.getInstance().getNodes();
        Log.d("Network","nodes: " + nodes.size());
        AddContactAdapter adapter = new AddContactAdapter(node, this , nodes);

        contactView.setLayoutManager(new LinearLayoutManager(this));
        contactView.setAdapter(adapter);

    }

    /**
     * Adds a link to the temporary ArrayList of links
     * @param link the link that will be added
     */
    public void addLink(Link link) {
        links.add(link);
    }
    /**
     * Removes a link from the temporary ArrayList of links
     * @param link the link that will be removed
     */
    public void removeLink(Link link) {
        links.remove(link);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_add_node:
                if(saveNode()){
                    Snackbar.make(contactView, "Node created!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    DataSource.getInstance().updateJSON();
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            case R.id.action_discard_node:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Saves the node and it's links.
     * @return true if all required fields were filled and it was saved otherwise false
     */
    private boolean saveNode() {

        TextView name = (TextView) findViewById(R.id.tv_name);
        TextView group = (TextView) findViewById(R.id.tv_group);
        TextView mail = (TextView) findViewById(R.id.tv_mail);
        TextView phone = (TextView) findViewById(R.id.tv_phone);
        TextView work = (TextView) findViewById(R.id.tv_work);

        if(name.getText().length() > 0){
            node.setData("name", name.getText().toString());
            node.setData("group", group.getText().toString());
            if(mail.getText().length() > 0) node.setData("mail", mail.getText().toString());
            if(phone.getText().length() > 0) node.setData("phone", phone.getText().toString());
            if(work.getText().length() > 0) node.setData("work", work.getText().toString());
            DataSource.getInstance().addNode(node, links);
            return true;
        }
        else{
            Snackbar.make(contactView, "You need to enter a name!", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
            return false;
        }
    }
}
