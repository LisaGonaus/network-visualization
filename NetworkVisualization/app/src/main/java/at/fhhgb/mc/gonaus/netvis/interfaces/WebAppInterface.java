package at.fhhgb.mc.gonaus.netvis.interfaces;

import android.app.Activity;
import android.content.Context;
import android.net.Network;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import at.fhhgb.mc.gonaus.netvis.MainActivity;
import at.fhhgb.mc.gonaus.netvis.R;
import at.fhhgb.mc.gonaus.netvis.data.DataSource;

/**
 * Created by Lisa on 09.05.2017.
 */

/**
 * This class represents the interface from JavaScript to the Android application.
 */
public class WebAppInterface {
    Context mContext;

    /** Instantiate the interface and set the context */
    public WebAppInterface(Context c) {
        mContext = c;
    }

    @JavascriptInterface
    public void deselectNode() {
        ((MainActivity)mContext).deselectNode();
    }

    /**
     * Sets the node with the given id as selected
     * @param id
     */
    @JavascriptInterface
    public void selectNode(int id) {
        Log.d("JS","select");
        ((MainActivity)mContext).selectNode(id);
    }

    /**
     * Reloads the data and sends it back to the visualization view
     * @param isTree true if hirachical json is needed
     */
    @JavascriptInterface
    public void requestData(boolean isTree) {
        DataSource data = DataSource.getInstance();
        Activity activity = ((Activity)mContext);

        if(data.hasChanged){
            data.updateJSON();
        }
        final String json = isTree ?  DataSource.getInstance().treeJson :  DataSource.getInstance().listJson;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WebView view = (WebView) ((Activity)mContext).findViewById(R.id.webview);
                view.loadUrl("javascript:updateData('"+ json +"')");
            }
        });

    }
}
