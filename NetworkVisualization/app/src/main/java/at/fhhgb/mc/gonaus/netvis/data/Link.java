package at.fhhgb.mc.gonaus.netvis.data;

import android.support.annotation.NonNull;

import java.util.ArrayList;

/**
 * Created by Lisa on 22.05.2017.
 * Stores a Link between two Nodes.
 */
public class Link implements Comparable<Link>{
    private Node node1;
    private Node node2;
    Long timestamp;
    int weight; // lower = better

    /**
     * Constructor for the class
     * @param p1 first node
     * @param p2 second node
     * @param weight weight of the relationship
     */
    public Link(Node p1, Node p2, int weight){
        setNode1(p1);
        setNode2(p2);
        this.weight = weight;
        timestamp = System.currentTimeMillis()/1000;
    }

    /**
     * Compares the link to another one.
     * @param r
     * @return 0 if the links contain the same nodes otherwise it returns -1
     */
    @Override
    public int compareTo(@NonNull Link r) {
        if( r.getNode1() == getNode1() && r.getNode2() == getNode2() ||
            r.getNode2() == getNode1() && r.getNode1() == getNode2()){
            return 0;
        }else{
            return -1;
        }
    }

    /**
     * This function checks whether one of the given nodes is contained within the link or not.
     * @param people nodes that will be checked
     * @return true if one of the nodes is contained in the connnection
     */
    public boolean contains(ArrayList<Node> people) {
        if(people != null) {
            for (Node p : people) {
                if (contains(p)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks whether a specific node is contained within the Conneciton.
     * @param p
     * @return true if the node is contained within the link
     */
    public boolean contains(Node p) {
        if(getNode1() == p || getNode2() == p){
            return true;
        }
        return false;
    }

    /**
     * Checks whether a node with any of the given ids is contained within the Conneciton.
     * @param visited the ids that will be checked
     * @return true if one of the ids matches one of the conneciton nodes
     */
    public boolean containsIds(ArrayList<Integer> visited) {
        if(visited != null) {
            for (int i : visited) {
                if (getNode1().getId() == i || getNode2().getId() == i) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks whether a node with the given id is contained within the Conneciton.
     * @param id
     * @return true if the id matches one of the conneciton nodes
     */
    public boolean contains(int id) {
        if (getNode1().getId() == id || getNode2().getId() == id) {
            return true;
        }
        return false;
    }

    /**
     * Returns the other contact to a given node that is included in the link.
     * @param other the node that is already known
     * @return the node it is connected to
     */
    public Node getContact(Node other){
        if(getNode1().getId() == other.getId()){
            return getNode2();
        }else{
            return getNode1();
        }
    }


    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Node getNode1() {
        return node1;
    }

    public void setNode1(Node node1) {
        this.node1 = node1;
    }

    public Node getNode2() {
        return node2;
    }

    public void setNode2(Node node2) {
        this.node2 = node2;
    }
}
