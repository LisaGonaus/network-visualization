package at.fhhgb.mc.gonaus.netvis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import at.fhhgb.mc.gonaus.netvis.adapter.MultiplePathAdapter;

/**
 * This Activity displays all found Paths in a list.
 */
public class PathViewActivity extends AppCompatActivity {
    private RecyclerView mPathView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_view);
        setTitle("Routes");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Routes");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPathView = (RecyclerView) findViewById(R.id.path_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mPathView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mPathView.setLayoutManager(mLayoutManager);
        mAdapter = new MultiplePathAdapter(this);
        mPathView.setAdapter(mAdapter);

    }
}
