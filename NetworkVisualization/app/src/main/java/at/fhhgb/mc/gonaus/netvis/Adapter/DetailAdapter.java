package at.fhhgb.mc.gonaus.netvis.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import at.fhhgb.mc.gonaus.netvis.R;
import at.fhhgb.mc.gonaus.netvis.data.Node;

/**
 * Created by Lisa on 17.06.2017.
 * This Adapter handles displaying the list of attributes a single person has.
 */

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.ViewHolder> {
    public enum hirachy{CONTACT_DETAILS, PERSONAL, OTHER};
    private Node mNode;
    private ArrayList<String> mKeys;

    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout mContent;
        public ViewHolder(LinearLayout v) {
            super(v);
            mContent = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public DetailAdapter(Node node, String[] keysToIgnore, Context c) {
        // mDataset = myDataset;
        mNode = node;
        mKeys = new ArrayList<String>(node.getData().keySet());

        for (int i = 0; i < keysToIgnore.length ; i++) {
            mKeys.remove(keysToIgnore[i]);
        }

        Collections.sort(mKeys,new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                int pos1 = getCategory(o1);
                int pos2 = getCategory(o2);

                if(pos1 == pos2) {
                    return o1.compareToIgnoreCase(o2);
                }else{
                    return pos1-pos2;
                }
            }

            private int getCategory(String s){
                if(s.contains("phone") || s.contains("mobile") || s.contains("mail")){
                    return 0;
                }else if(s.contains("work") || s.contains("home")){
                    return 1;
                }
                return 2;
            }
        });
        mContext = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_entry, parent, false);

        // set the view's size, margins, paddings and layout parameters
        //...
        DetailAdapter.ViewHolder vh = new DetailAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(DetailAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        String key = mKeys.get(position);
        String value = mNode.getData(key);

        ImageView icon = (ImageView) holder.mContent.findViewById(R.id.detail_entry_img);
        int imgId = R.drawable.ic_info;
        if(key.contains("phone")){
            imgId = R.drawable.ic_smartphone;
        }else if(key.contains("mail")){
            imgId = R.drawable.ic_mail;
        }else if(key.contains("work")){
            imgId = R.drawable.ic_work;
        }
        icon.setImageDrawable(mContext.getResources().getDrawable(imgId));

        TextView title = (TextView) holder.mContent.findViewById(R.id.detail_entry_val);
        title.setText(value);

        TextView subtitle = (TextView) holder.mContent.findViewById(R.id.detail_entry_desc);
        key = key.substring(0, 1).toUpperCase() + key.substring(1);
        subtitle.setText(key);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mKeys.size();
    }
}
