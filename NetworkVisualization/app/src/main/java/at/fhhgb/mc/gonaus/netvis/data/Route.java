package at.fhhgb.mc.gonaus.netvis.data;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.ArrayList;

/**
 * Created by Lisa on 10.06.2017.
 */

public class Route implements Comparable<Route> {
    public static int maxWeight = 3*DataSource.maxWeight;
    public static int maxNodes = 4;
    private ArrayList<Node> nodes = new ArrayList<>();
    private ArrayList<Integer> nodeWeights = new ArrayList<>();
    private int weight = 0;

    public Node getNode(int index){
        if(index >= 0 && index < nodes.size()){
            return nodes.get(index);
        }
        return null;
    }


    public int getNodeWeight(int index) {
        if(index >= 0 && index < nodeWeights.size()){
            return nodeWeights.get(index);
        }
        return 0;
    }

    public int getLength(){
        return nodes.size();
    }

    public Route(Route r) {
        if (r != null) {
            weight = r.weight;
            for (int i= 0; i<r.nodes.size(); i++) {
                addNode(r.nodes.get(i),r.nodeWeights.get(i));
            }
        }
    }

    public boolean addNode(Node node, int cost) {
        if (node != null) {
            nodes.add(node);
            nodeWeights.add(cost);
            weight += cost;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Weight[" + weight + "]: ");
        for (Node node : nodes) {
            s.append(node.getId() + "=" + node.getData("name"));
            if (nodes.size()-1 != nodes.lastIndexOf(node)) {
                s.append(" ==> ");
            }
        }
        return s.toString();
    }

    public static ArrayList<Route> findRoutes(ArrayList<Route> routes, Route route, Node start, Node goal, ArrayList<Integer> visited) {
        //ArrayList<Route> routes = new ArrayList<Route>();
        route = new Route(route);
        // exit if route too long or the start equals the goal
        if (route.nodes.size() > maxNodes || route.weight > maxWeight ) return routes;
        if (route.nodes.size() == 0) route.addNode(start, 0);
        if (visited == null) visited = new ArrayList<>();

        Queue<Link> queue = new PriorityQueue<>(DataSource.getInstance().network.getLinks(start.getId(), visited));
        visited.add(start.getId());

        while (!queue.isEmpty()) {
            Link link = queue.poll();
            // search the nodes children if the node isn't already within the route;
            Node node = start.getId() != link.getNode1().getId() ? link.getNode1() : link.getNode2();
            //if (!visited.contains(node.getId())) {
                Route next = new Route(route);
                if (node == goal) {
                    next.addNode(node, link.weight);
                    if(next.nodes.size() <= maxNodes && next.weight < maxWeight){
                        routes.add(next);
                    }
                }
                else{
                    next.addNode(node, link.weight);
                    findRoutes(routes, next, node, goal, new ArrayList<Integer>(visited));
                }
            //}
        }
        Collections.sort(routes);
        return routes;
    }

    @Override
    public int compareTo(@NonNull Route r) {
        if (r.weight != weight || r.nodes.size() != nodes.size()) {
            return weight - r.weight + (nodes.size() - r.nodes.size());
        } else {
            for (int i = 0; i < r.nodes.size(); i++) {
                if (r.nodes.get(i).getId() != nodes.get(i).getId()) {
                    return -1;
                }
            }
        }
        return 0;
    }

}
