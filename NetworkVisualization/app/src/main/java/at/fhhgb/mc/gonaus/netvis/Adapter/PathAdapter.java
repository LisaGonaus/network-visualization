package at.fhhgb.mc.gonaus.netvis.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import at.fhhgb.mc.gonaus.netvis.*;
import at.fhhgb.mc.gonaus.netvis.R;
import at.fhhgb.mc.gonaus.netvis.data.Link;
import at.fhhgb.mc.gonaus.netvis.data.DataSource;
import at.fhhgb.mc.gonaus.netvis.data.Node;
import at.fhhgb.mc.gonaus.netvis.data.Route;

/**
 * Created by Lisa on 17.06.2017.
 * This Adapter displays a single path/route.
 */

public class PathAdapter extends RecyclerView.Adapter<PathAdapter.ViewHolder> {

    private Route mRoute;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout mContent;
        public ViewHolder(LinearLayout v) {
            super(v);
            mContent = v;
        }
    }

    public PathAdapter(Route route, Context c) {
        mRoute = route;
        mContext = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PathAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.path_entry, parent, false);


        // set the view's size, margins, paddings and layout parameters
        //...
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Node n = mRoute.getNode(position);

        ImageView symbol = (ImageView) holder.mContent.findViewById(R.id.path_node_img);
        ImageView link = (ImageView) holder.mContent.findViewById(R.id.path_node_link);
        TextView linkDesc = (TextView) holder.mContent.findViewById(R.id.path_node_link_desc);
        TextView title = (TextView) holder.mContent.findViewById(R.id.path_node_name);

        link.setVisibility(View.VISIBLE);
        linkDesc.setVisibility(View.VISIBLE);

        int symbolRes = R.drawable.path_node;
        String name = n.getData("name");
        if(position == 0){
            name = "You";
            symbolRes = R.drawable.path_node_start;
        }
        if (position == mRoute.getLength()-1){
            symbolRes = R.drawable.path_node_end;
            link.setVisibility(View.GONE);
            linkDesc.setVisibility(View.GONE);
        } else {
            String desc = DataSource.getInstance().getLinkTypeFromValue(new Link(mRoute.getNode(position),null,mRoute.getNodeWeight(position+1)));
            linkDesc.setText(desc);
        }

        ImageButton btn = (ImageButton)holder.mContent.findViewById(R.id.path_btn_details);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetails(position);
            }
        });

        symbol.setImageDrawable(mContext.getResources().getDrawable(symbolRes));

        title.setText(name);

    }

    private void showDetails(int position) {
        int id = mRoute.getNode(position).getId();
        Intent intent = new Intent(mContext, NodeDetailActivity.class);
        intent.putExtra(DataSource.MSG_NODE_ID, id);
        mContext.startActivity(intent);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mRoute.getLength();
    }
}

