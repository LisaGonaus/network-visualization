package at.fhhgb.mc.gonaus.netvis.converter;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.w3c.dom.Document;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by Lisa on 26.05.2017.
 * This class handles the storage access.
 */

public class FileHandler {
    private static final String TAG = "FileHandler";
    public static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL = 1;
    private Context context;

    /**
     * Initializes the FileHandler with a given Context.
     * @param c
     */
    public FileHandler(Context c) {
        context = c;
    }

    //--------------------------- Asset Folder -----------------------------//

    /**
     * Reads a file from the assets folder
     *
     * @param path File path
     * @return InputStream
     */
    public InputStream getFileFromAssets(String path) {
        try {
            return context.getAssets().open(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //------------------------- External Storage ----------------------------//

    /**
     * Stores a String of data to a file located in the external storage.
     *
     * @param path File path
     * @param data File content
     * @return True if the file was successfully created.
     */
    public boolean writeFileToExternal(String path, Document data) {
        if (isExternalStorageWriteable()) {
            try {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), path);
                if(!file.exists()){
                    file.createNewFile();
                }
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                Result output = new StreamResult(file);
                Source input = new DOMSource(data);

                transformer.transform(input, output);

                return true;
            }  catch (TransformerException | IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(TAG, "No external storage accessible.");
        }
        return false;
    }


    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWriteable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}
