package at.fhhgb.mc.gonaus.netvis;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import at.fhhgb.mc.gonaus.netvis.adapter.ContactAdapter;
import at.fhhgb.mc.gonaus.netvis.adapter.DetailAdapter;
import at.fhhgb.mc.gonaus.netvis.data.DataSource;
import at.fhhgb.mc.gonaus.netvis.data.Node;

/**
 * An Activity that shows
 */
public class NodeDetailActivity extends AppCompatActivity {
    /**
     * Attributes of a node that will not be shown in the attribute listing.
     */
    static String[] mStringsToIgnore = new String[] {"name", "id"};
    /**
     * The node that will be shown.
     */
    Node node;
    RecyclerView mDetailView;
    RecyclerView mContactView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        int id = intent.getIntExtra(DataSource.MSG_NODE_ID, 0);
        node = DataSource.getInstance().network.getNode(id);
        if(node != null){
            setTitle(node.getData("name"));
        }

        if(node != null){
            node.setData("group", "Work");
            node.setData("work", "Example Corporation");
            node.setData("phone number", "02757 1234");
            node.setData("mobile phone", "0660 12345678");
            node.setData("email", node.getData("name")+"@gmail.com");
        }

        mDetailView = (RecyclerView)findViewById(R.id.node_detail_view);
        mDetailView.setLayoutManager(new LinearLayoutManager(this));
        mDetailView.setAdapter(new DetailAdapter(node, mStringsToIgnore, this));

        mContactView = (RecyclerView)findViewById(R.id.node_contact_view);
        mContactView.setLayoutManager(new LinearLayoutManager(this));
        mContactView.setAdapter(new ContactAdapter(node, this));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if(node != DataSource.getInstance().network.getRoot()){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Snackbar.make(view, "Delete the node?", Snackbar.LENGTH_LONG)
                            .setAction("DELETE",new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    DataSource.getInstance().deleteNode(node);
                                    Snackbar.make(view, "Node deleted!", Snackbar.LENGTH_SHORT).show();
                                    navigateUp();
                                }
                            }).show();
                }
            });
        }else{
            fab.setVisibility(View.GONE);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }

    /**
     * Navigates to the previous view.
     */
    public void navigateUp(){
        super.onBackPressed();
    }
}
