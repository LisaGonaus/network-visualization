package at.fhhgb.mc.gonaus.netvis.converter;

import android.util.Log;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import at.fhhgb.mc.gonaus.netvis.data.Link;
import at.fhhgb.mc.gonaus.netvis.data.DataSource;
import at.fhhgb.mc.gonaus.netvis.data.Network;
import at.fhhgb.mc.gonaus.netvis.data.Node;

/**
 * Created by Lisa on 26.05.2017.
 */

/**
 * This class can parse graphml containing undirected graphs with edgeWeight and additional node data fields.
 * Anything else is ignored.
 */
public class GraphMLParser {
    public static final String ns = null;
    private boolean isRoot = false;

    /**
     * Generates the key definition tag
     *
     * @param doc    DOM Document that is used to create the tag.
     * @param id     Variable name/id
     * @param parent DOM Element on which the key is used (node, edge)
     * @param type   Data type of the variable
     * @return
     */
    private Element generateKey(Document doc, String id, String parent, String type) {
        Element key = doc.createElement("key");
        key.setAttribute("id", id);
        key.setAttribute("for", parent);
        key.setAttribute("attr.name", id);
        key.setAttribute("attr.type", type);
        return key;
    }

    /**
     * Generates a valid xml document from the given network
     * @param network
     * @return an xml Document containing the network
     */
    public Document export(Network network) {

        try {
            //Create instance of DocumentBuilderFactory
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            //Create blank DOM Document
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();
            doc.setXmlVersion("1.0");

            // create the root element and set the namespace
            Element root = doc.createElement("graphml");
            root.setAttribute("xmlns", "http://graphml.graphdrawing.org/xmlns");
            doc.appendChild(root);

            // creates the graph tag
            Element graph = doc.createElement("graph");
            graph.setAttribute("edgedefault", "undirected");
            doc.getFirstChild().appendChild(graph);
            //add a comment
            graph.appendChild(doc.createComment("Graph Data (Undirected)"));

            //TODO: add key definitions
            graph.appendChild(generateKey(doc, "weight", "edge", "int"));

            //Enumeration<Integer> nodeKeys = network.nodes.keys();
            Hashtable<Integer,Node> nodes = network.getNodes();
            ArrayList<Integer> nodeKeys = new ArrayList<>(nodes.keySet());
            Collections.sort(nodeKeys);
            for( int nodeKey : nodeKeys) {
                //add the nodes
                Node data = nodes.get(nodeKey);
                Element node = doc.createElement("node");

                //Add the id of the child
                node.setAttribute("id", "" + nodeKey);

                //define whether the node is root or not
                Element nodeData = doc.createElement("data");
                nodeData.setAttribute("key", "root");
                nodeData.setTextContent((data == network.getRoot())+"");
                node.appendChild(nodeData);
                Enumeration<String> keys = data.getData().keys();

                // add additional data
                while (keys.hasMoreElements()) {
                    nodeData = doc.createElement("data");
                    String key = keys.nextElement();
                    nodeData.setAttribute("key", key);
                    nodeData.setTextContent(data.getData(key));
                    node.appendChild(nodeData);
                }
                graph.appendChild(node);
            }
            //add the links
            int i = 0;
            for (Link con : network.getLinks()) {
                Log.d("Export", "con["+ (i++) + "]" + con.getNode1() + " -->" + con.getNode2());
                Element edge = doc.createElement("edge");
                edge.setAttribute("source", "" + con.getNode1().getId());
                edge.setAttribute("target", "" + con.getNode2().getId());
                // add edge weight
                Element weight = doc.createElement("data");
                weight.setAttribute("key", "weight");
                weight.setTextContent("" + con.getWeight());
                edge.appendChild(weight);

                graph.appendChild(edge);
                Log.d("Export", "con-count" + graph.getChildNodes().getLength());
            }

            return doc;
        }
        catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Reads a network from an input stream and parses the data in it.
     * @param stream InputStream to the data
     * @return the read network
     */
    public Network parse(InputStream stream) {
        Network n = new Network();
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(stream, null);
            parser.nextTag();

            parser.require(XmlPullParser.START_TAG, ns, "graphml");
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                if (parser.getName().equals("graph")) {
                    readGraph(parser, n);
                } else {
                    skip(parser);
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return n;
    }

    // ----------------------------------- Parse Helper Funcitons -------------------------------//

    private void readGraph(XmlPullParser parser, Network n) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "graph");
        //Todo: handle directed network
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("node")) {
                Node node = readNode(parser);
                n.getNodes().put(node.getId(), node);
                if(isRoot){
                    n.setRoot(node);
                    isRoot = false;
                }
            } else if (name.equals("edge")) {
                String[] edge = readEdge(parser);
                Link con = new Link(n.getNode(Integer.parseInt(edge[0])), n.getNode(Integer.parseInt(edge[1])), Integer.parseInt(edge[2]));
                n.getLinks().add(con);
            } else {
                skip(parser);
            }
        }
    }

    private String[] readEdge(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "edge");
        String source = parser.getAttributeValue(ns, "source");
        String target = parser.getAttributeValue(ns, "target");
        // TODO: change default to 0 or 1
        String weight = ""+(int)Math.ceil(Math.random()* DataSource.maxWeight);
        //String weight = "5";
        while (parser.next() != XmlPullParser.END_TAG) {
            String name = parser.getName();
            if (name != null) {
                if (name.equals("data")) {
                    String[] data = readData(parser);
                    if (data[0].equals("weight") || data[0].equals("edgeWeight")) {
                        weight = data[1];
                    }
                }
            }
        }
        parser.require(XmlPullParser.END_TAG, ns, "edge");
        return new String[]{source, target, weight};
    }

    private Node readNode(XmlPullParser parser) throws XmlPullParserException, IOException {
        Node p = new Node();
        parser.require(XmlPullParser.START_TAG, ns, "node");
        String id = parser.getAttributeValue(ns, "id");
        p.setId(Integer.parseInt(id));

        while (parser.next() != XmlPullParser.END_TAG) {
            String name = parser.getName();
            if (name != null) {
                if (name.equals("data")) {
                    String[] data = readData(parser);
                    if(data[0].equals("root") && data[1].equals("true")){
                        isRoot = true;
                    }else{
                        p.setData(data[0], data[1]);
                    }
                }
            }
        }
        parser.require(XmlPullParser.END_TAG, ns, "node");
        return p;
    }

    // For the tags title and summary, extracts their text values.
    private String[] readData(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        parser.require(XmlPullParser.START_TAG, ns, "data");
        String key = parser.getAttributeValue(ns, "key");
        String value = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "data");
        return new String[]{key, value};
    }

    // For the tags title and summary, extracts their text values.
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
