package at.fhhgb.mc.gonaus.netvis.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import at.fhhgb.mc.gonaus.netvis.R;
import at.fhhgb.mc.gonaus.netvis.data.Link;
import at.fhhgb.mc.gonaus.netvis.data.DataSource;
import at.fhhgb.mc.gonaus.netvis.data.Node;

/**
 * Created by Lisa on 17.06.2017.
 * This Adapter displays the contacts of a person.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {
    private Node mNode;
    private ArrayList<Link> mContacts;

    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout mContent;
        public ViewHolder(LinearLayout v) {
            super(v);
            mContent = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ContactAdapter(Node node, Context c) {
        mNode = node;
        mContacts = DataSource.getInstance().network.getLinks(node.getId(), new ArrayList<Integer>());
        Collections.sort(mContacts, new Comparator<Link>() {
            @Override
            public int compare(Link o1, Link o2) {
                return o1.getWeight()- o2.getWeight();
            }
        });
        mContext = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ContactAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.node_contact_entry, parent, false);

        // set the view's size, margins, paddings and layout parameters
        //...
        ContactAdapter.ViewHolder vh = new ContactAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ContactAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Link con = mContacts.get(position);
        Node n = con.getContact(mNode);
        int weight = con.getWeight();

        TextView name = (TextView) holder.mContent.findViewById(R.id.node_contact_name);
        name.setText(n.getData("name"));

        ImageButton btn = (ImageButton) holder.mContent.findViewById(R.id.node_contact_btn_remove);
        btn.setClickable(true);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteLink(con);
            }
        });

        TextView subtitle = (TextView) holder.mContent.findViewById(R.id.node_contact_desc);
        subtitle.setText(DataSource.getInstance().getLinkTypeFromValue(con));
    }

    private void deleteLink(Link con) {
        mContacts.remove(con);
        DataSource.getInstance().deleteLink(con);
        notifyDataSetChanged();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mContacts.size();
    }
}

