package at.fhhgb.mc.gonaus.netvis.data;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Lisa on 22.05.2017.
 * This class a Singleton and the main data source of the application.
 */
public class DataSource {

    private static DataSource instance = null;
    /**
     * @return The singular instance of the class.
     */
    public static DataSource getInstance() {
        if (instance == null) {
            instance = new DataSource();
        }
        return instance;
    }
    private DataSource() {
        network = new Network();
    }

    /**
     * Stores the network.
     */
    public Network network;
    /**
     * Stores the routes that were calculated.
     */
    public ArrayList<Route> currentRoutes = new ArrayList<>();
    /**
     * True if the data was changed.
     */
    public boolean hasChanged = true;
    /**
     * Contains the hirachical json representation of the network
     */
    public String treeJson = "";
    /**
     * Contains the node and link list json representation of the network
     */
    public String listJson = "";

    // Constants
    public final static String MSG_NODE_ID = "at.fhhgb.mc.gonaus.node.id";
    public final static int IMPORT_FILE = 111;

    // Visualization URLs
    public final static String PATH_FORCE = "file:///android_asset/force/force.html";
    public final static String PATH_TREE = "file:///android_asset/tree/tree.html";
    public final static String PATH_ARC = "file:///android_asset/chord/chord.html";

    // Max Link Weight
    public final static int maxWeight = 4;
    // String representations of the relationships
    public static String[] linkValues = new String[]{"Acquaintances", "Colleagues", "Friends", "Close Friends"};
    //public static String[] linkValues = new String[]{"Dislikes", "Knows", "Likes"};

    // Observer
    private ArrayList<DataObserver> observer = new ArrayList<>();
    public void addObserver(DataObserver dataObserver){
        observer.add(dataObserver);
    }

    /**
     * Updates the json data if anything has changed.
     */
    public void updateJSON() {
        if(hasChanged){
            listJson = network.getJson();
            treeJson = network.getTreeJson();
            hasChanged = false;
            for ( DataObserver d: observer ) {
                d.dataChanged();
            }
        }
    }

    /**
      * Replace the network with a new one
      * @param network
     */
    public void setNetwork(Network network) {
        this.network = network;
        hasChanged = true;
    }

    /**
     * Adds a node to the network
     * @param node
     * @param links
     */
    public void addNode(Node node, ArrayList<Link> links) {
        network.addPerson(node);
        network.getLinks().addAll(links);
        hasChanged = true;
    }

    /**
     * Deletes a node of the network
     * @param node
     */
    public void deleteNode(Node node){
        for ( DataObserver d: observer ) {
            d.nodeDeleted(node.getId());
        }
        deleteNode(node);
        hasChanged = true;
    }

    /**
     * Adds a link to the network
     * @param con
     */
    public void addLink(Link con) {
        network.getLinks().add(con);
        hasChanged = true;
        for ( DataObserver d: observer ) {
            d.linkAdded(con.getNode1().getId(), con.getNode2().getId());
        }
    }

    /**
     * Deletes a link
     * @param con
     */
    public void deleteLink(Link con) {
        network.getLinks().remove(con);
        hasChanged = true;
        for ( DataObserver d: observer ) {
            d.linkDeleted(con.getNode1().getId(), con.getNode2().getId());
        }
    }

    /**
     * Returns the string representation of the link weight
     * @param con
     * @return
     */
    public String getLinkTypeFromValue(Link con) {
        //int descId = (int)Math.round((1 - ((double)con.weight/maxWeight))*(linkValues.length-1));
        return linkValues[maxWeight-con.weight];
    }

    /**
     * @return All nodes within the network
     */
    public Hashtable<Integer, Node> getNodes() {
        return network.getNodes();
    }

    /**
     * Resets the network to a clean state.
     */
    public void clearNetwork() {
        Node.nodeCount = 0;
        network = null;
        listJson = "";
        treeJson = "";
    }
}
