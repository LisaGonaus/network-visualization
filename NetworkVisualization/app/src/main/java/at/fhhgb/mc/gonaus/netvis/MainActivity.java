package at.fhhgb.mc.gonaus.netvis;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Document;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import at.fhhgb.mc.gonaus.netvis.data.DataSource;
import at.fhhgb.mc.gonaus.netvis.converter.FileHandler;
import at.fhhgb.mc.gonaus.netvis.converter.GraphMLParser;
import at.fhhgb.mc.gonaus.netvis.data.Node;
import at.fhhgb.mc.gonaus.netvis.data.Route;
import at.fhhgb.mc.gonaus.netvis.data.DataObserver;
import at.fhhgb.mc.gonaus.netvis.interfaces.WebAppInterface;

/**
 * The Main Activity of this Application.
 */
public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, DataObserver {
    public final static String TAG = "Network Test";

    LinearLayout container;
    Button btnDetails, btnSearch, btnDelete;
    TextView tvPlaceholer;
    WebView webView;
    int selectedTab = 0;
    Node selectedNode = null;
    DataSource data = DataSource.getInstance();
    //TODO: preserve webview state

    // ------------------------- Initialization -----------------------------------//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("");
        data.addObserver(this);

        TabLayout tabBar = (TabLayout) findViewById(R.id.tabLayout);
        tabBar.addOnTabSelectedListener(this);

        initButtons();
        initWebView();
        initData();

        tvPlaceholer = (TextView) findViewById(R.id.tv_placeholder);
        tvPlaceholer.setVisibility(View.GONE);
    }

    /**
     * Initializes the mock data
     */
    private void initData(){
        FileHandler fh = new FileHandler(this);
        if (data.network.isEmpty()) {
            data.setNetwork(new GraphMLParser().parse(fh.getFileFromAssets("network.xml")));
            data.updateJSON();
        }
    }

    /**
     * Initializes the WebView
     */
    private void initWebView(){
        webView = (WebView) findViewById(R.id.webview);
        webView.addJavascriptInterface(new WebAppInterface(this), "android");
        WebSettings webSettings = webView.getSettings();
        // enable javascript & file access for javascript
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        // disable scroll end animation
        webView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        // initialize graph once the page is loaded
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                data.updateJSON();
                String json;
                if (selectedTab == 0) {
                    json = data.listJson;
                } else {
                    json = data.treeJson;
                }
                webView.loadUrl("javascript:init('" + json + "')");
            }
        });
    }

    /**
     * Initialises all needed Buttons
     */
    private void initButtons(){
        ImageButton btnImport = (ImageButton) findViewById(R.id.btn_import);
        btnImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                importNetwork();
            }
        });

        ImageButton btnExport = (ImageButton) findViewById(R.id.btn_export);
        btnExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exportNetwork();
            }
        });

        Button btnAddNode = (Button) findViewById(R.id.btn_add_node);
        btnAddNode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCreateNode();
            }
        });

        container = (LinearLayout) findViewById(R.id.btn_container);
        container.setVisibility(View.GONE);

        btnDetails = (Button) findViewById(R.id.btn_show_node);
        btnDetails.setEnabled(false);
        btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetails();
            }
        });

        btnSearch = (Button) findViewById(R.id.btn_search_routes);
        btnSearch.setEnabled(false);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    public void run() {
                        // a potentially  time consuming task
                        Node root = data.network.getRoot();
                        data.currentRoutes.clear();
                        Route.findRoutes(data.currentRoutes, null, root, selectedNode, null);
                        Log.d(TAG, "Routes: " + data.currentRoutes.size());
                        showRoutes();
                    }
                }).start();
            }
        });

        btnDelete = (Button) findViewById(R.id.btn_delete_node);
        btnDelete.setEnabled(false);
        btnDelete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v == btnDelete) {
                    if (selectedNode != null) {
                        webView.loadUrl("javascript:deleteNode('" + selectedNode.getId() + "')");
                        data.network.deleteNode(selectedNode);
                        selectedNode = null;
                        btnDetails.setEnabled(false);
                        btnDelete.setEnabled(false);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    // -------------------------- Navigation ---------------------------------------//

    /**
     * Navigates to the Add Node View
     */
    private void showCreateNode() {
        Intent intent = new Intent(this, AddNodeActivity.class);
        startActivity(intent);
    }


    /**
     * Navigates to the Path View
     */
    private void showRoutes() {
        Intent intent = new Intent(this, PathViewActivity.class);
        startActivity(intent);
    }

    /**
     * Navigates to the Detail View
     */
    private void showDetails() {
        Intent intent = new Intent(this, NodeDetailActivity.class);
        intent.putExtra(DataSource.MSG_NODE_ID, selectedNode.getId());
        startActivity(intent);
    }

    /**
     * Loads the visualization for the given tab into the WebView.
     */
    private void loadVisualization() {
        deselectNode();
        switch (selectedTab) {
            case 0:
                webView.loadUrl(DataSource.PATH_FORCE);
                break;
            case 1:
                webView.loadUrl(DataSource.PATH_TREE);
                break;
            case 2:
                webView.loadUrl(DataSource.PATH_ARC);
                break;
        }
    }

    // ---------------------------- Tab Selection ----------------------------------//
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        selectedTab = tab.getPosition();
        loadVisualization();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    // --------------------------- Node Selection ---------------------------------//

    /**
     * Sets the selected node.
     * @param id id of the node
     */
    public void selectNode(int id) {
        selectedNode = data.network.getNode(id);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                container.setVisibility(View.VISIBLE);
                btnDetails.setEnabled(true);
                btnSearch.setEnabled(true);
                if(selectedNode.getId() != data.network.getRoot().getId()){
                    btnDelete.setEnabled(true);
                }else{
                    btnDelete.setEnabled(false);
                }
            }
        });
    }

    /**
     * Discards the node selection
     */
    public void deselectNode() {
        selectedNode = null;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                container.setVisibility(View.GONE);
                btnDetails.setEnabled(false);
                btnSearch.setEnabled(false);
                btnDelete.setEnabled(false);
            }
        });
    }

    // --------------------------- Data Changes -----------------------------------//

    @Override
    public void nodeDeleted(final int id) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:deleteNode('" + id + "')");
            }
        });

        selectedNode = null;
        btnDetails.setEnabled(false);
        Snackbar.make(this.webView, "Node was Deleted", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void linkDeleted(final int id1, final int id2) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:deleteLink(" + id1 +","+ id2+ ")");
            }
        });
    }

    @Override
    public void linkAdded(final int id1,final int id2) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:addLink(" + id1 +","+ id2+ ")");
            }
        });
    }

    @Override
    public void dataChanged() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadVisualization();
            }
        });
    }

    // --------------------------- Import/Export -----------------------------------//

    /**
     * Exports the network to an GraphML file.
     */
    private void exportNetwork() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Document doc = new GraphMLParser().export(DataSource.getInstance().network);
            if (new FileHandler(this).writeFileToExternal("network.xml", doc)) {
                Snackbar.make(this.webView, "Network saved to Documents as 'network.xml'.", Snackbar.LENGTH_LONG).show();
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    FileHandler.PERMISSIONS_REQUEST_WRITE_EXTERNAL);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case FileHandler.PERMISSIONS_REQUEST_WRITE_EXTERNAL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    exportNetwork();
                } else {
                    Snackbar.make(this.webView, "Permission denied: File could not be saved.", Snackbar.LENGTH_LONG).show();
                }
                return;
            }
            default: {
                Snackbar.make(this.webView, "Permission denied.", Snackbar.LENGTH_LONG).show();
                return;
            }
        }
    }

    /**
     * Imports a network from a GraphML file.
     */
    private void importNetwork() {
        Intent intent = new Intent().setType("text/xml").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select a file"), DataSource.IMPORT_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== DataSource.IMPORT_FILE && resultCode==RESULT_OK) {
            Uri selectedfile = data.getData(); //The uri with the location of the file
            Log.d(TAG,selectedfile.getPath());

            try {
                tvPlaceholer.setVisibility(View.GONE);
                FileInputStream input = (FileInputStream) getContentResolver().openInputStream(selectedfile);

                this.data.clearNetwork();
                this.data.setNetwork(new GraphMLParser().parse(input));
                this.data.updateJSON();
                Snackbar.make(this.webView, "File was successfully loaded!", Snackbar.LENGTH_SHORT).show();
            } catch (FileNotFoundException e) {
                Snackbar.make(this.webView, "Invalid File!", Snackbar.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

}
